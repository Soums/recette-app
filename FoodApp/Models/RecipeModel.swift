//
//  File.swift
//  FoodApp
//
//  Created by Soums on 10/06/2022.
//

import Foundation

enum Category: String, CaseIterable, Identifiable {
    var id: String { self.rawValue }
    case appetizer = "Entrée"
    case dish = "Plat"
    case dessert = "Dessert"
}

struct Recipe: Identifiable {
    let id = UUID()
    let name: String
    let image: String
    let category: Category.RawValue
    let description: String
    let ingredients: String
    let preparations: String
    let datePublished: String
}

extension Recipe {
    static let all: [Recipe] = [
     Recipe(
        name: "Entrée au chocolat doux de Suisse",
        image: "https://pbs.twimg.com/media/BtAv_6bIEAAfNZt.jpg",
        category: "Entrée",
        description: "dndjd",
        ingredients: "jjr",
        preparations: "Tout d'abord",
        datePublished: "2022-01-01"
     )
    ]
}
