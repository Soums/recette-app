//
//  AddRecipeView.swift
//  FoodApp
//
//  Created by Soums on 20/06/2022.
//

import SwiftUI

struct AddRecipeView: View {
    @EnvironmentObject var recipesVM: RecipesViewModel
    
    @State private var name: String = ""
    @State private var image: String = ""
    @State private var selectedCategory: Category = Category.appetizer
    @State private var description: String = ""
    @State private var ingredients: String = ""
    @State private var preparations: String = ""
    
    @State private var navigateToRecipe = false
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Nom")) {
                    TextField("Nom recette", text: $name)
                }
                
                Section(header: Text("Url image")) {
                    TextEditor(text: $image)
                }
                
                Section(header: Text("Category")) {
                     Picker("Category", selection: $selectedCategory) {
                        ForEach(Category.allCases) { category in
                            Text(category.rawValue)
                                .tag(category)
                        }
                    }
                    .pickerStyle(.menu)
                }
                
                Section(header: Text("Description")) {
                    TextEditor(text: $description)
                }
                
                Section(header: Text("Ingrédient(s)")) {
                    TextEditor(text: $ingredients)
                }
                
                Section(header: Text("Preparation(s)")) {
                    TextEditor(text: $preparations)
                }
            }
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        dismiss()
                    } label: {
                        Label("Annuler", systemImage: "xmark")
                            .labelStyle(.iconOnly)
                    }
                }
                
                ToolbarItem {
                    NavigationLink(isActive: $navigateToRecipe) {
                        RecipeView(recipe: recipesVM.recipes.sorted{ $0.datePublished > $1.datePublished }[0])
                            .navigationBarBackButtonHidden(true)
                    } label: {
                        Button {
                            saveRecipe()
                            navigateToRecipe = true
                        } label: {
                            Label("Ajouter", systemImage: "checkmark")
                                .labelStyle(.iconOnly)
                        }
                    }
                    .disabled(name.isEmpty)
                }
            })
            .navigationTitle("Nouvelle recette")
            .navigationBarTitleDisplayMode(.inline)
        }
        .navigationViewStyle(.stack)
    }
}

struct AddRecipeView_Previews: PreviewProvider {
    static var previews: some View {
        AddRecipeView()
            .environmentObject(RecipesViewModel())
    }
}

extension AddRecipeView {
    private func saveRecipe() {
        let dateNow = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-mm-yyyy"
        
        let datePublished = dateFormatter.string(from: dateNow)
        print(datePublished)
        
        let recipe = Recipe(name: name, image: image, category: selectedCategory.rawValue, description: description, ingredients: ingredients, preparations: preparations, datePublished: datePublished)
        
        recipesVM.addRecipe(recipe: recipe)
    }
}
