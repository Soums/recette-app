//
//  NewRecipe.swift
//  FoodApp
//
//  Created by Soums on 10/06/2022.
//

import SwiftUI

struct NewRecipeView: View {
    @State private var showAddRecipe = false
    
    var body: some View {
        NavigationView {
            Button("Ajouter une recette") {
                showAddRecipe = true
            }
                .navigationTitle("Nouvelle recette")
        }
        .navigationViewStyle(.stack)
        .sheet(isPresented: $showAddRecipe) {
            AddRecipeView()
        }
    }
}

struct NewRecipe_Previews: PreviewProvider {
    static var previews: some View {
        NewRecipeView()
    }
}
